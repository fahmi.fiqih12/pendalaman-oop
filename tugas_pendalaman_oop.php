<?php

trait hewanFight{
    public  $nama,
            $darah = "50",
            $jumlahKaki,
            $keahlian;

}

trait fightHewan{
    public $attackPower,
           $defencePower;
}

abstract class Hewan{
    use hewanFight;

    public abstract function atraksi();
}

abstract class Fight{
    use fightHewan;

    public abstract function serang();
    public abstract function diserang();

}


class Elang { 
    use hewanFight, fightHewan;

    public function __construct($jumlahKaki = 0 , $keahlian = "keahlian",  $attackPower = 0, $defencePower = 0 ){
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function detail(){
        $str = "Elang : jumlah kaki : {$this->jumlahKaki}, keahlian : {$this->keahlian}, attackPower : {$this->attackPower},
         defencePower : {$this->defencePower} ";
        return $str;
    }

    public function setNama($nama){
        return $this->nama = $nama;    
    }
    
    public function atraksi(){
        $str = "{$this->nama}__1 sedang menyerang {$this->keahlian}__3";
        return $str;
    }

    public function serang(){
        return $this->atraksi();
    }

    public function diserang(){
      return $this->darah - $this->attackPower / $this->defencePower;
    }

    public function getInfoHewan(){
        $str = "{$this->nama}, {$this->darah}, {$this->jumlahKaki}, {$this->keahlian}, {$this->attackPower},
        {$this->defencePower}";
        return $str;
    }

}

class Harimau {
    use hewanFight, fightHewan;

    public function __construct($jumlahKaki = 0 , $keahlian = "keahlian",  $attackPower = 0, $defencePower = 0 ){
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    
    public function detail(){
        $str = "Harimau : jumlah kaki : {$this->jumlahKaki}, keahlian : {$this->keahlian}, attackPower : {$this->attackPower},
         defencePower : {$this->defencePower} ";
        return $str;
    }

    
    public function setNama($nama){
        return $this->nama = $nama;    
    }
    
    public function atraksi(){
        $str = "{$this->nama}__1 sedang menyerang {$this->keahlian}__3";
        return $str;
    }
    
    public function serang(){
        return $this->atraksi();
    }

    public function diserang(){
        return $this->darah - $this->attackPower / $this->defencePower;
    }

    public function getInfoHewan(){
        $str = "{$this->nama}, {$this->darah}, {$this->jumlahKaki}, {$this->keahlian}, {$this->attackPower},
        {$this->defencePower}";
        return $str;
    }


}


$obj = new Elang(2, "terbang tinggi", 10, 5);
echo $obj->detail();
echo "<br>";
$obj->setNama("elang");
echo $obj->atraksi();
echo "<br>";
echo $obj->serang();
echo "<br>";
echo $obj->diserang();


echo "<br>";
echo "<br>";

$obj2 = new Harimau(4, "Lari Cepeat", 7, 8);
echo $obj2->detail();
echo "<br>";
$obj2->setNama("Harimau");
echo $obj2->atraksi();
echo "<br>";
echo $obj2->serang();
echo "<br>";
echo $obj->diserang();

echo "<hr>";
echo $obj->getInfoHewan();
echo "<br>";
echo $obj2->getInfoHewan();